import React from 'react'
import Paper from '@material-ui/core/Paper'
import Grid from '@material-ui/core/Grid'
import { connect } from 'react-redux'
import { initQuiz } from '../actions/Quizactions'
import { withStyles } from '@material-ui/styles'
import _ from 'underscore'

import QuestionTopProgress from './QuestionTopProgress'
import QuestionInfo from './QuestionInfo'

const styles = {
  root: {
    flexGrow: 1,
    marginLeft: 240,
    marginRight: 240,
    marginTop: 20,
    marginBottom: 20,
  },
  paper: {
    padding: 10,
    textAlign: 'center'
  }
}

const Loader = () => {
  return (
    <div>
      <div className='loadercontainer'>
        <div className='loaderflex'>
          <div className='loader'>
            &nbsp;
          </div>
        </div>
        <div className='load-text'>
            Loading...
        </div>
      </div>
    </div>
  )
}

class QuizBoard extends React.Component {

  componentDidMount () {
    this.props.initQuiz()
  }

  
  render () {
    const { classes ,activeQuestion ,loading } = this.props;

    if (loading || !_.has(activeQuestion, 'category')) {
      return <Loader />
    }

    return (
      <div className={classes.root} >
        <Grid container >
          <Grid item xs={12}>
            <Paper className={classes.paper}>
              <QuestionTopProgress />
              <QuestionInfo />
            </Paper>
          </Grid>
        </Grid>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return { ...state.QuizReducer }
}

const mapDispatchToProps = ({
  initQuiz: initQuiz
})

QuizBoard = withStyles(styles)(QuizBoard)
QuizBoard = connect(mapStateToProps, mapDispatchToProps)(QuizBoard)
export default QuizBoard