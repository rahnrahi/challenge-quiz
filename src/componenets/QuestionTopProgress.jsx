import React from 'react'
import { connect } from 'react-redux'
import { lighten, withStyles } from '@material-ui/core/styles'
import LinearProgress from '@material-ui/core/LinearProgress'

const BorderLinearProgress = withStyles({
  root: {
    height: 30,
    backgroundColor: lighten('#ff6c5c', 0.5)
  },
  bar: {
    borderRadius: 40,
    backgroundColor: '#ff6c5c'
  }
})(LinearProgress)

function QuestionTopProgress (props) {
  let { activeQuestionIndex, questionsLenght } = props
  let percentprogress = (activeQuestionIndex / questionsLenght) * 100

  return (
    <div>
      <BorderLinearProgress
        variant='determinate'
        color='primary'
        value={percentprogress} />
    </div>
  )
}

const mapStateToProps = (state) => {
  return ({
    activeQuestionIndex: state.QuizReducer.activeQuestionIndex,
    questionsLenght: state.QuizReducer.questions.length
  })
}

const mapDispatchToProps = ({
})

let QuestionTopProgressConnect = connect(mapStateToProps, mapDispatchToProps)(QuestionTopProgress)
export default QuestionTopProgressConnect
