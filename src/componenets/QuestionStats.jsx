import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import { connect } from 'react-redux'

const useStyles = makeStyles(theme => ({
  miniBarProgress: {
    backgroundColor: '#8a898a',
    height: '100%',
    position: 'absolute',
    top: '0rem',
    left: '0rem'
  },
  miniBar: {
    border: '1px solid #8a898a',
    position: 'relative',
    marginRight: '0.5rem',
    height: '35px',
    borderRadius: '12px',
    overflow: 'hidden',
    width: '100%'
  }
}))

function QuestionStats (props) {
  const classes = useStyles()
  const { maxPercent, correctanswerPercent, minPercent } = props

  return (
    <div className={classes.miniBar}>
      <div className={classes.miniBarProgress} style={{ width: `${maxPercent}%`, 'backgroundColor': '#ccc' }}>&nbsp;</div>
      <div className={classes.miniBarProgress} style={{ width: `${correctanswerPercent}%`, 'backgroundColor': '#616161' }}>&nbsp;</div>
      <div className={classes.miniBarProgress} style={{ width: `${minPercent}%`,'backgroundColor': '#000' }}>&nbsp;</div>
    </div>
  )
}

const mapStateToProps = (state) => {
  return { ...state.QuizReducer }
}

const mapDispatchToProps = ({
})

let QuestionStatsConnect = connect(mapStateToProps, mapDispatchToProps)(QuestionStats)
export default QuestionStatsConnect
