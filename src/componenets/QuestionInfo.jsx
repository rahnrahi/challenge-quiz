import React from 'react'
import { connect } from 'react-redux'
import { selectQuestion, SET_NEXT_QUESTION, initQuiz } from '../actions/Quizactions'
import { Typography, Grid, Button } from '@material-ui/core'
import Rating from '@material-ui/lab/Rating'
import QuestionStats from './QuestionStats'
import { makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    paddingLeft: 80,
    paddingRight: 80,
    paddingTop: 80,
    textAlign: 'left'
  },
  answerRoot: {
    flexGrow: 1,
    padding: 0,
    position: 'relative',
    minHeight: 380
  },
  answerGridItem: {
    paddingTop: 20
  },
  textRight: {
    textAlign: 'right'
  },
  answerInfo: {
    textAlign: 'center',
    minHeight: 150
  },
  answerStats: {
    bottom: 0,
    textAlign: 'center'
  },
  highLghtCorrectButton: {
    backgroundColor: 'green'
  },
  topInfo: {
    marginBottom: 20
  },
  questionTitle: {
    marginBottom: 20,
    minHeight: 100
  },
  fab: {
    margin: theme.spacing(1),
    minWidth: 300
  }

}))

let AnswerItem = props => {
  const { index, activeQuestion, item, classes } = props
  const { selected } = activeQuestion

  let disabled = selected && !item.selected && !item.is_correct
  let color = item.is_correct ? 'primary' : 'secondary'
  let highLghtCorrect = selected && item.is_correct
  let buttonclasses = !highLghtCorrect ? [classes.fab] : [classes.fab, classes.highLghtCorrectButton]

  return (
    <Grid key={index} className={['answer-grid', classes.answerGridItem].join(' ')} item xs={6}>

      {selected ? (<Button variant='contained' disabled={disabled} color={color} aria-label='delete' className={buttonclasses.join(' ')}>
        {item.answer_Text}
      </Button>
      ) : (<Button variant='contained' onClick={() => props.selectQuestion(index)} color='primary' aria-label='delete' className={classes.fab}>
        {item.answer_Text}
      </Button>)
      }
    </Grid>
  )
}

function QuestionInfo (props) {
  const { activeQuestion, activeQuestionIndex, questionsLength } = props
  const classes = useStyles()
  const { maxPercent, correctanswerPercent } = props
  return (
    <Grid className={classes.root} container>
      <Grid className={classes.topInfo} item xs={12}>
        <Typography variant='h4' component='h3'>
            Question {activeQuestionIndex} of {questionsLength}
        </Typography>
        <Typography component='p'>
          {activeQuestion.category}
        </Typography>
        <Rating value={activeQuestion.rating} readOnly />
      </Grid>

      <Grid className={classes.questionTitle} item xs={12}>
        <Typography variant='h5' gutterBottom>
          {activeQuestion.question}
        </Typography>
      </Grid>
      <Grid className={classes.answerRoot} container>
        {activeQuestion.answers.map((item, k) => {
          return <AnswerItem
            selectQuestion={props.selectQuestion}
            index={k} key={k} activeQuestion={activeQuestion}
            classes={classes} item={item} />
        })}

        <Grid className={classes.answerInfo} item xs={12}>
          {activeQuestion.selected && (<p>{activeQuestion.answered_correctly ? (<span>Correct!</span>) : (<span>Sorry!</span>)}</p>)}

          {activeQuestion.selected && questionsLength > activeQuestionIndex &&
                (<React.Fragment>
                  <Button variant='contained' onClick={() => props.goTonextQuestion()} aria-label='delete' className={classes.fab}>
                      Next Question
                  </Button>
                </React.Fragment>
                )
          }

          {activeQuestion.selected && questionsLength === activeQuestionIndex && (
            <React.Fragment>
              <Button variant='contained' onClick={() => props.initQuiz()} aria-label='delete' className={classes.fab}>
                    Start Over
              </Button>
            </React.Fragment>
          )}
        </Grid>

        <Grid item xs={6}>
                Score: {correctanswerPercent}%
        </Grid>

        <Grid className={classes.textRight} item xs={6}>
                Max Score: {maxPercent}%
        </Grid>
        <Grid className={classes.answerStats} item xs={12}>
          <QuestionStats />
        </Grid>
      </Grid>
    </Grid>
  )
}

const mapStateToProps = (state) => {
  return ({
    activeQuestion: state.QuizReducer.activeQuestion,
    activeQuestionIndex: state.QuizReducer.activeQuestionIndex,
    questionsLength: state.QuizReducer.questions.length,
    maxPercent: state.QuizReducer.maxPercent,
    correctanswerPercent: state.QuizReducer.correctanswerPercent,
    minPercent: state.QuizReducer.minPercent
  })
}
const mapDispatchToProps = ({
  selectQuestion: selectQuestion,
  initQuiz: initQuiz,
  goTonextQuestion: () => {
    return function (dispatch, getState) {
      dispatch(({ type: SET_NEXT_QUESTION }))
    }
  }
})

let QuestionInfoConnect = connect(mapStateToProps, mapDispatchToProps)(QuestionInfo)
export default QuestionInfoConnect
