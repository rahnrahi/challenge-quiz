import _ from 'underscore'
import {
  SET_QUESTIONS,
  REQUEST_QUESTIONS,
  SET_NEXT_QUESTION,
  SELECT_QUESTION,
  SET_QUESTION_STATS
} from '../actions/Quizactions'

let initstate = {
  questions: [],
  loading: true,
  activeQuestionIndex: 0,
  activeQuestion: {},
  minPercent: 0,
  maxPercent: 0,
  correctanswerPercent: 0
}

export default function QuizReducer (state = {
  ...initstate
}, action) {
  switch (action.type) {
    case REQUEST_QUESTIONS:
      return {
        ...initstate
      }

    case SET_QUESTIONS:
      state['questions'] = [...action.questions].map(q => decodeQuestionItem(q))
      state['loading'] = false
      return {
        ...state
      }

    case SELECT_QUESTION:
      return selectQuestion(state, action)

    case SET_QUESTION_STATS:
      return setQuestionStats(state, action)

    case SET_NEXT_QUESTION:
      return returnNextQuestion(state, action)

    default:
      return {
        ...state
      }
  }
}

let setQuestionStats = (state, action) => {
  let questions = [...state.questions]
  let answered = _.where(questions, {
    selected: true
  })

  let CorrectAnswered = _.where(questions, {
    answered_correctly: true
  })

  let totalAnsweredlength = answered.length
  let correctLength = CorrectAnswered.length
  let remainingLength = questions.length - totalAnsweredlength

  state.minPercent = Number((correctLength / questions.length) * 100).toFixed(2)
  state.maxPercent = Number(((remainingLength + correctLength) / questions.length) * 100).toFixed(2)
  let correctanswerPercent = Number(((correctLength) / totalAnsweredlength) * 100)
  correctanswerPercent = isNaN(correctanswerPercent) ? 0 : correctanswerPercent
  state.correctanswerPercent = Number(correctanswerPercent).toFixed(2)

  return {
    ...state
  }
}

let selectQuestion = (state, action) => {
  let questions = [...state.questions]
  let activeQuestionIndex = state.activeQuestionIndex
  let activeQuestion = {
    ...state.activeQuestion
  }
  let answered = {
    ...activeQuestion.answers[action.answerIdex]
  }
  answered['selected'] = true
  activeQuestion['selected'] = true
  activeQuestion['answered_correctly'] = answered.is_correct

  activeQuestion.answers[action.answerIdex] = answered
  questions[--activeQuestionIndex] = activeQuestion

  state['questions'] = questions
  state['activeQuestion'] = activeQuestion
  return {
    ...state
  }
}

let returnNextQuestion = (state, action) => {
  let questions = [...state.questions]
  let activeQuestionIndex = state.activeQuestionIndex
  let activeQuestion = {
    ...questions[state.activeQuestionIndex]
  }

  state['activeQuestion'] = activeQuestion
  state['activeQuestionIndex'] = ++activeQuestionIndex

  return {
    ...state
  }
}

let decodeQuestionItem = qItem => {
  let question = {
    ...qItem
  }
  question['selected'] = false
  question['answered_correctly'] = false
  question['category'] = decodeURIComponent(question['category'])
  question['question'] = decodeURIComponent(question['question'])
  let correctAnswer = decodeURIComponent(question['correct_answer'])

  // make correct and incorrect answers in single array  of objects
  let answers = question['incorrect_answers'].map(v => {
    let q = {}
    q['answer_Text'] = decodeURIComponent(v)
    q['is_correct'] = false
    q['selected'] = false
    return q
  })

  answers.push({
    answer_Text: correctAnswer,
    is_correct: true,
    selected: false
  })

  answers = _.shuffle(answers) // suffle array to put correct answer to random position

  let difficulty = question['difficulty']
  let rating = 0

  if (difficulty === 'hard') rating = 3
  else if (difficulty === 'easy') rating = 1
  else if (difficulty === 'medium') rating = 2

  question['rating'] = rating
  question['answers'] = answers
  return question
}
