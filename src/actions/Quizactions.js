export const SET_QUESTIONS = 'SET_QUESTIONS'
export const REQUEST_QUESTIONS = 'REQUEST_QUESTIONS'
export const SET_NEXT_QUESTION = 'SET_ACTIVE_QUESTION'
export const SELECT_QUESTION = 'SELECT_QUESTION'
export const SET_QUESTION_STATS = 'SET_QUESTION_STATS'
let questions = require('../questions.json')

export function initQuiz () {
  return function (dispatch, getState) {
    dispatch(({ type: REQUEST_QUESTIONS }))
    dispatch(({ type: SET_QUESTIONS, questions: [...questions] }))
    dispatch(({ type: SET_NEXT_QUESTION }))
    dispatch(({ type: SET_QUESTION_STATS }))
  }
}

export function selectQuestion (answerIdex) {
  return function (dispatch, getState) {
    dispatch(({ type: SELECT_QUESTION, answerIdex: answerIdex }))
    dispatch(({ type: SET_QUESTION_STATS }))
  }
}
